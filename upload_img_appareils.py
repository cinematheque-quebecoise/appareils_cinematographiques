# -*- coding: utf-8  -*-

import sys

import pywikibot
from pywikibot.specialbots import UploadRobot
from pywikibot import family
import json
from os import path

class Family(family.Family):
    def __init__(self):
        family.Family.__init__(self)
        self.name = 'commons'
        self.langs = {
            'en': 'localhost:8080',
        }

    def scriptpath(self, code):
        return {
            'en': '/commons',
        }[code]

    def version(self, code):
        return {
            'en': u'1.23.2',
        }[code]

# family = 'commons'
# mylang = 'commons'
# usernames['commons']['commons'] = 'koslambrou'
def generate_commons_description(nac, desc, date, categories):
    return u"""{{Information
|Description    = {{fr|1=""" + desc + """}}
|Source         = {{Institution:Cinémathèque québécoise}}
|Author         = {{Institution:Cinémathèque québécoise}}
|Date           = {{date|""" + str(date['year']) + """}}
|Permission     = {{cc-by-sa-4.0}}
|Other fields   = {{Information field |name=Accession number|value=""" + nac + """}},{{Credit line |Author = © [[Institution:Cinémathèque québécoise|Cinémathèque québécoise]] | Other = WC/TITLE |License = cc-by-sa-4.0}}
|other_versions =
}}
[[Category:Appareils Cinémathèque québécoise]]
[[Category:Uploaded with pyWikiBot]]
""" + categories

#DorianeGray
# https://pywikipedia-l.wikimedia.narkive.com/Fn0NHWlE/pywikibot-on-localhost
def upload_commons(filename, pagetitle, description):
    url = [ filename ]
    keepFilename = False        #set to True to skip double-checking/editing destination filename
    verifyDescription = True    #set to False to skip double-checking/editing description => change to bot-mode
    # targetSite = pywikibot.getSite('commons', 'commons')
    # targetSite = pywikibot.Site('commons', 'commons')
    # targetSite = pywikibot.Site(url="http://localhost:8080/commons/index.php/Main_Page")
    targetSite = pywikibot.Site(code="en", fam=Family())

    bot = UploadRobot(url, description=description, useFilename=pagetitle, keepFilename=keepFilename, verifyDescription=verifyDescription, targetSite=targetSite)
    bot.run()

def complete_desc_and_upload(filename, pagetitle, nac, desc, date, categories):
    description = generate_commons_description(nac, desc, date, categories)
    upload_commons(filename, pagetitle, description)

def main(args):
    #list each file here

    # removedSpecialChars = urllib.parse.quote_plus(title.replace(' ', '_'), safe='')

    # filename    = """thermopompe-1.jpg"""
    # pagetitle   = """testimage-1-from asdfasdfa.jpg"""
    # desc        = """Mount St Helens viewed from ... in the rain"""
    # date        = "2010-04-07"
    # categories  = """[[Category:Locality]]
# [[Category:Theme]]
# [[Category:View type]]
# [[Category:Feature1]]
# [[Category:Feature2]]"""
    # complete_desc_and_upload(filename, pagetitle, desc, date, categories)

    ##sample with:  - local file name identical to file name at Commons
    ##              - date as previous file
    ##              - less quotes (no CR or " in fields)
    #filename   = "testimage-2.jpg"
    #pagetitle  = filename
    #desc       = "Mount St Helens as seen from ... at sunset"
    #categories = "[[Category:Locality]] [[Category:Theme]] [[Category:View type]] [[Category:Feature1]] [[Category:Feature2]]"
    #complete_desc_and_upload(filename, pagetitle, desc, date, categories)

    finished_titles = []
    if path.exists('data/finished_titles'):
        with open('data/finished_titles') as fobj:
            for line in fobj:
                finished_titles.append(line.strip())

    with open('data/finished_titles', 'a') as wobj:
        with open('data/wikicommons_data.json') as fobj:
            all_commons_data = json.load(fobj)

            unfinished_uploads = [d for d in all_commons_data if d['title'] not in finished_titles]
            for i, commons_data in enumerate(unfinished_uploads):

                print(commons_data)

                description = generate_commons_description(
                    commons_data['nac'], commons_data['description'], commons_data['date'], '')

                print(description)
                # upload_commons(
                #     commons_data['img_path'],
                #     commons_data['title'],
                #     description)

                wobj.write(commons_data['title'])
                wobj.write('\n')

                break

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    finally:
        pywikibot.stopme()
