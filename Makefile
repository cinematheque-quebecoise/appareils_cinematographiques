VERSION := 0.1
GITLAB_PROJECT_ID := 25083708
GITLAB_TOKEN := $(shell cat .gitlab-token)

convert:
	python convert.py

data/appareils_cinematographiques.csv: convert.py
data/appareils_cinematographiques.json: convert.py
data/appareils_cinematographiques_schema.json: convert.py
	python convert.py

release: data/appareils_cinematographiques.csv data/appareils_cinematographiques.json data/appareils_cinematographiques_schema.json data/NUM_appareils.zip
	./create-release.sh \
		"appareils cinématographiques v$(VERSION)" \
		"v$(VERSION)" \
		$(GITLAB_PROJECT_ID) \
		"Nouvelle publication v$(VERSION)" \
		$(GITLAB_TOKEN) \
		data/appareils_cinematographiques.csv \
		data/appareils_cinematographiques.json \
		data/appareils_cinematographiques_schema.json \
		data/NUM_appareils.zip
