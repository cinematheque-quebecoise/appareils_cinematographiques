{ pkgs ? import <nixpkgs> {}}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    python38
    python38Packages.pandas
    python38Packages.exifread
    python38Packages.requests
    python38Packages.xmltodict
    gcc
    jq
  ];
}
