import pandas as pd
import json
import numpy as np
from genson import SchemaBuilder
import exifread
import platform
import os
import datetime
import requests
import csv
# from xml.etree import ElementTree
import xmltodict
import urllib.parse

# wiki bot: koslambrou@cq_appareils_televerser:0m4mu1qamguhcfovu8ho74vq3gvl903j

def creation_date(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime

def get_date_taken(path):
    with open(path, 'rb') as fh:
        # tags = exifread.process_file(fh, stop_tag="EXIF DateTimeOriginal")
        tags = exifread.process_file(fh)
        print(tags)
        dateTaken = tags["EXIF ProfileDateTime"]
        return dateTaken

field_descriptions = {
    'NAC': 'Numéro d\'accession unique de l\'article',
    'NC': 'Emplacement de l\'objet.',
    'COB': 'Classifications générales de l\'article enregistré, les ensembles les plus vastes qui caractérise l’objet. Elles renvoient à sa fonctionnalité première.',
    'SCOB': 'Les sous-catégories qui sont au deuxième niveau de la classification générale de l\'objet',
    'OB': 'Le nom normalisé de l\'article.',
    'TOB': 'Les qualificatifs descriptifs de l\'article',
    'NOB': 'Le nombre d\'objets décrits dans l\'enregistrement',
    'OBASS': 'Références aux articles ou séries d’objet qui possèdent un lien nécessaire entre eux.',
    'ELEM': 'Le nom des parties constituantes d\'un objet, c\'est-à-dire ses éléments détachables de l\'objet.',
    'NELEM': 'Le nombre de parties composantes d\'un objet connu',
    'TI': 'Titre du document.',
    'AR': 'Les noms des affichistes, des directeurs artistiques, des compositeurs, des paroliers, des animateurs, des photographes, des scénaristes et autres personnes qui sont responsables de la conception, de la sélection ou de la fabrication de l\'article, autrement dit qui en sont considérés comme l’auteur.',
    'ATEC': 'Le nom des autres personnes, interprètes ou techniciens qui, sans en être les créateurs, ont directement contribué à la conception de l\'article, ou par extension au film auquel l\'article se rattache',
    'FAB': 'Les noms principaux des fabricants qui sont responsable de la fabrication de l\'article',
    'PYF': 'Pays',
    'PRF': 'Province du fabricant',
    'MQ': 'Les marques de fabrique de l\'objet',
    'MODL': 'Les noms et numéros du modèle attribués à l’appareil',
    'DADD': 'Date de début de diffusion',
    'DFP': 'La date de fin de production de l\'article',
    'HT': 'La mesure de la hauteur de l\'article mesurée de la base au sommet',
    'LA': 'La mesure de la largeur de l\'article mesurée à l’horizontale en se plaçant devant la face principale de l’objet',
    'LO': 'La mesure de la longueur exacte de l\'article. Elle s’applique à des objets qui n’ont pas de face principale',
    'PROF': 'La mesure de la profondeur d’un article en trois dimensions possédant une face principale',
    'DIEX': 'La mesure du diamètre extérieur d’un article de forme ronde ou arrondie',
    'UNL': 'Unité de métrage des mesures',
    'MA': 'Les matériaux ayant servi à la fabrication d\'un objet en trois dimensions',
    'TF': 'Les procédés, les méthodes ou les techniques de fabrication de l\'objet en trois dimensions.',
    'MED': 'Le nom du ou des médiums ayant servi à la création de l\'objet en deux dimensions',
    'SUP': 'Les matériaux utilisés comme support pour une pièce en deux dimensions.',
    'VED': 'Descriptions limitées du sujet de l\'artefact.',
    'DESN': 'Noms des personnes à qui appartiennent l\'objet.',
    'IMPE': 'URLs de l\'image de l\'objet',
    'VIGNE': 'URLs de la vignette de l\'objet'
}

columns_to_drop = ['NC', 'VIGNE']

def convert_fields_to_list(fields):
    def f(obj):
        for field in fields:
            if field in obj and obj[field]:
                obj[field] = obj[field].split('; ')
        return obj
    return f

def find_array_fields(objs, exceptions=[]):
    array_fields = set()
    for obj in objs:
        for k in obj:
            if k not in exceptions and not pd.isna(obj[k]) and obj[k] and '; ' in str(obj[k]):
                array_fields.add(k)
    return array_fields

def remove_empty_fields(obj):
    for k in list(obj.keys()):
        if obj[k] == None:
            obj.pop(k, 'None')
    return obj

def convert_cq_images_to_wiki_commons(obj, titles):
    commons_url = 'https://commons.wikimedia.org/wiki/File:'
    if 'IMPE' in obj and obj['IMPE'] is not None:
        obj['IMPE'] = []
        for title in titles:
            # removedSpecialChars = title
            # removedSpecialChars = title.translate ({ord(c): '' for c in '!@#$%^&*()[]{};:,./<>?\|`~-=_+'})
            removedSpecialChars = urllib.parse.quote_plus(title.replace(' ', '_'), safe='')
            obj['IMPE'].append(commons_url + removedSpecialChars.replace(' ', '_'))
    return obj

def convert_appareils_to_dict(appareils_tsv):
    appareils_tsv['DADD'].astype(str)

    appareils_tsv.dropna(axis=0, how='any', thresh=None, subset=None, inplace=False)
    dropped_appareils = appareils_tsv.drop(columns=columns_to_drop)
    appareils_no_nan = dropped_appareils.where((pd.notnull(dropped_appareils)), None)

    appareils_dicts = appareils_no_nan.to_dict(orient='records')
    for a in appareils_dicts:
        for k in a:
            if (k == 'NOB' or k == 'NELEM') and a[k] is not None:
                a[k] = int(a[k])

    fields_with_array = find_array_fields(appareils_dicts, exceptions=['MQ'])

    appareils_to_list = list(map(convert_fields_to_list(fields_with_array), appareils_dicts))
    appareils_to_list = list(map(remove_empty_fields, appareils_to_list))

    return appareils_to_list

def create_schema(appareils_dict):
    builder = SchemaBuilder()
    builder.add_schema({'type': 'array', 'description': 'Liste d\'appareils enregistrés dans la Cinémathèque québécoise'})
    builder.add_object(appareils_dict)
    builder.to_schema()

    schema = json.loads(builder.to_json())
    for prop in schema['items']['properties']:
        schema['items']['properties'][prop]['description'] = field_descriptions[prop]
    return schema

def create_imgs_data(appareils):
    def create_img_data(ap):
        titleparts = []
        if 'MQ' in ap:
            titleparts.append(ap['MQ'])
        elif 'FAB' in ap:
            titleparts.append(' '.join(ap['FAB']))
        if 'MODL' in ap:
            titleparts.append(' '.join(ap['MODL']))
        titleparts.append(ap['OB'])

        year = ap['NAC'].split('.')[0]
        title = f'{" ".join([t for t in titleparts if "?" not in t])}' #{year}
        if '(' in title and ')' not in title:
            title = title.replace('(', '')
        if ')' in title and '(' not in title:
            title = title.replace(')', '')

        description_lines = [title]
        if 'HT' in ap:
            description_lines.append(f'Hauteur: {str(ap["HT"])}{ap["UNL"]}')
        if 'LA' in ap:
            description_lines.append(f'Largeur: {str(ap["LA"])}{ap["UNL"]}')
        if 'LO' in ap:
            description_lines.append(f'Longueur: {str(ap["LO"])}{ap["UNL"]}')
        description_lines.append('Collection de la Cinémathèque québécoise.')

        description = '. '.join(description_lines)

        data = []
        numberOfPaths = len(ap['IMPE'])
        for i, path in enumerate(ap['IMPE']):
            # Quelques inconsistances dans les chemin d'images
            img_path = 'data/' + '/'.join(path.replace('NUM_Appareils', 'NUM_appareils')
                                              .replace('IMAGM_appareils', 'IMAG\\NUM_appareils')
                                              .split('\\')[1:])
            creationtimestamp = creation_date(img_path)
            # creationtimestamp = creation_date("/home/kolam/Dropbox/images/arbresyntaxique.png")
            creationdate = datetime.datetime \
                .fromtimestamp(creationtimestamp) \
                .strftime('%Y-%m-%d') \
                .split('-')
            new_title = title
            if numberOfPaths > 1:
                new_title = f'{title} {str(i + 1)}'

            data.append({
                'img_path': img_path,
                'nac': ap['NAC'],
                'title': f'{new_title} Cinémathèque québécoise {ap["NAC"].replace(".", "").replace("-", "")}.{(img_path[::-1].split(".")[0][::-1]).lower()}',
                'description': description,
                'date': {
                    'year': int(creationdate[0]),
                    'month': int(creationdate[1]),
                    'day': int(creationdate[2])
                }
            })

        convert_cq_images_to_wiki_commons(ap, [d['title'] for d in data])

        return data

    data = []
    for aps in list(map(create_img_data, filter(lambda ap: 'IMPE' in ap,
                                               appareils))):
        for ap in aps:
            data.append(ap)
    return data

def main():
    # Convert data to JSON
    appareils_tsv = pd.read_csv('appareils.TXT', delimiter='\t', encoding='UTF-8')
    appareils = convert_appareils_to_dict(appareils_tsv)

    appareils_csv = appareils
    for appareil in appareils_csv:
        for field in appareil:
            if type(appareil[field]) == type([]):
                appareil[field] = '; '.join(appareil[field])

    with open('data/appareils_cinematographiques.csv', 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=[d for d in field_descriptions if d not in columns_to_drop])
        writer.writeheader()
        for appareil in appareils_csv:
            writer.writerow(appareil)

    # Create schema
    schema = create_schema(appareils)
    with open('data/appareils_cinematographiques_schema.json', 'w') as wobj:
        wobj.write(json.dumps(schema, indent=4, ensure_ascii=False))

    # imgs_data = create_imgs_data(appareils)
    # with open('data/wikicommons_data.json', 'w') as wobj:
    #     wobj.write(json.dumps(imgs_data, indent=4, ensure_ascii=False))

    # non_unique_titles = []
    # params = { 'format': 'json' }
    # for img_data in imgs_data:
    #     url = 'https://commons.wikimedia.org/w/api.php?action=query&titles=Image:{img_data["title"]}'
    #     r = requests.get(url=url, params=params)
    #     if '-1' not in r.json()['query']['pages']:
    #         non_unique_titles.append(img_data)
    # print('Nombre de titre qui existe déjà dans Commons:', len(non_unique_titles))
    # for title in non_unique_titles:
    #     print(title)

    # for ap in appareils:
    #     convert_cq_images_to_wiki_commons(ap)

    with open('data/appareils_cinematographiques.json', 'w') as wobj:
        wobj.write(json.dumps(appareils, indent=4, ensure_ascii=False))

if __name__ == '__main__':
    main()
